<?php

namespace App;

class Post extends MyModel{

	public function getHashtags() {
		$hs = explode(' ', $this->hashtags);
		$h = '';
		foreach ($hs as $item) {
			$h .= '<a href="/search?hash='.$item.'">#' . $item . '</a> ';
		}
		return $h;
	}

	public static function getByHash() {
		$hash = $_GET['hash'];
		$posts_all = self::all();
		$posts = [];
		foreach ($posts_all as $item) {
			$hashtags = explode(' ', $item->hashtags);
			if(in_array($hash, $hashtags)){
				$posts[] = $item;
			}
		}
		return $posts;
	}

	public static function getByHashtags() {
		$hashs = explode(' ', trim($_GET['hashtags']));
		$posts_all = self::all();
		$posts = [];
		foreach ($posts_all as $item) {
			$hashtags = explode(' ', $item->hashtags);
			foreach ($hashs as $hash) {
				if(in_array($hash, $hashtags)){
					$posts[] = $item;
				}
			}
		}
		return $posts;
	}

	public static function getByKw() {
		$keywords = $_GET['keywords'];
		return self::where('desc', 'like', '%'.$keywords.'%')
								->orWhere('data', 'like', '%'.$keywords.'%')->get();
	}
}
