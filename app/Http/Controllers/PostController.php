<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use PDF;
use App\{
	User,
	Post
};

class PostController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Add() {
		return view('post.add')->with([
			'user' => User::curr(),
		]);
	}
	public function Edit($id) {
		$model = Post::getBy('id', $id);
		return view('post.edit')->with([
			'model' => $model,
		]);
	}
	public function Delete($id) {
		Post::where('id', $id)->delete();
		return redirect()->to('/');
	}
	public function Create(Request $request) {
		$model = new Post();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->data = request()->data;
		$model->hashtags = request()->hashtags;
		$model->user_id = User::curr()->id;

		$model->save();
		return redirect()->to('/post/view/' . $model->id);
	}
	public function Update($id, Request $request) {
		$model = Post::getBy('id', $id);

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->data = request()->data;
		$model->hashtags = request()->hashtags;

		$model->save();
		return redirect()->to('/post/view/'.$model->id);
	}
}
