<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Post
};

class SiteController extends Controller
{

	public function __construct(){
		// $this->middleware('auth');
	}

	public function Index() {
		return view('index')->with([
			'user' => User::curr(),
			'posts' => Post::all(),
		]);
	}

	public function Post($id) {
		$post = Post::getBy('id', $id);
		return view('post')->with([
			'post' => $post,
			'user' => User::curr(),
		]);
	}

	public function Search() {
		$posts = [];
		if (isset($_GET['hash'])) $posts = Post::getByHash();
		if (isset($_GET['hashtags'])) $posts = Post::getByHashtags();
		if (isset($_GET['keywords'])) $posts = Post::getByKw();

		return view('index')->with([
			'user' => User::curr(),
			'posts' => $posts,
		]);
	}

	public function Users() {
		$users = User::all();
		return view('users')->with([
			'users' => $users,
		]);
	}
}
