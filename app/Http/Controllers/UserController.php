<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User
};

class UserController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Add() {
		$this->the_before();
		return view('user.add')->with([
			'cur_role' => $this->cur_role,
			'roles' => User::getRoles(),
		]);
	}
	public function Edit($id) {
		$this->the_before();
		$model = User::getBy('id', $id);
		return view('user.edit')->with([
			'cur_role' => $this->cur_role,
			'roles' => User::getRoles(),
			'model' => $model,
		]);
	}
	public function Delete($id) {
		$this->the_before();
		User::where('id', $id)->delete();
		return redirect()->to('/users');
	}
	public function Create(Request $request) {
		$this->the_before();
		$m = new User();

		$m->name = request()->name;
		$m->full_name = request()->full_name;
		$m->role = request()->role;
		$m->email = request()->email;
		$m->password = bcrypt(request()->password);

		$m->save();
		return redirect('/users');
	}
	public function Update($id, Request $request) {
		$this->the_before();
		$m = User::getBy('id', $id);

		$m->name = request()->name;
		$m->full_name = request()->full_name;
		$m->role = request()->role;
		$m->email = request()->email;
		if(trim(request()->password) !== '') $m->password = bcrypt(request()->password);

		$m->save();
		return redirect()->to('/users');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
