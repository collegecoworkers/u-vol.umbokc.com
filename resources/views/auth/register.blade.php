@extends('../layouts.auth')
@section('content')
<h2>Регистрация</h2>
<div class="row" row>
	<section col=three></section>
	<section class="6u">
		<form method="post" action="{{ route('register') }}">
			{{ csrf_field() }}
			<div class="row ">
				<div class="12u">
					<input name="full_name" placeholder="Полное имя" type="text" class="text" />
					@if ($errors->has('full_name'))<span class="help-block"><strong c#f>{{ $errors->first('full_name') }}</strong></span>@endif
					<br>
					<input name="name" placeholder="Логин" type="text" class="text" />
					@if ($errors->has('name'))<span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span>@endif
					<br>
					<input name="email" placeholder="Email" type="text" class="text" />
					@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span>@endif
					<br>
					<input name="password" placeholder="Пароль" type="password" class="text" />
					@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span>@endif
					<br>
					<input name="password_confirmation" placeholder="Пароль еще раз" type="password" class="text" />
					@if ($errors->has('password_confirmation'))<span class="help-block"><strong c#f>{{ $errors->first('password_confirmation') }}</strong></span>@endif
				</div>
			</div>
			<div class="row half">
				<div class="12u">
					<ul class="actions" ta:c>
						<li><button type="submit" class="button" cur:p>Отправить</button></li>
						<li><a href="{{ route('login') }}">Войти</a></li>
					</ul>
				</div>
			</div>
		</form>
	</section>
	<section col=three></section>
</div>
@endsection

