@extends('../layouts.auth')
@section('content')
<h2>Вход</h2>
<div class="row" row>
	<section col=three></section>
	<section class="6u">
		<form method="post" action="{{ route('login') }}">
			{{ csrf_field() }}
			<div class="row ">
				<div class="12u">
					<input name="email" placeholder="Email" type="text" class="text" />
					@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span>@endif
					<br>
					<input name="password" placeholder="Пароль" type="password" class="text" />
					@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span>@endif
				</div>
			</div>
			<div class="row half">
				<div class="12u">
					<ul class="actions">
						<li><button type="submit" class="button" cur:p>Отправить</button></li>
						<li><a href="{{ route('register') }}">Регистрация</a></li>
					</ul>
				</div>
			</div>
		</form>
	</section>
	<section col=three></section>
</div>
@endsection
