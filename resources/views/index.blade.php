@extends('layouts.app')
@section('content')
<div class="row">
	<section class="8u ">
		<div class="row">
			<div class="u12">
				@if (empty($posts))
					<p>Посто нет</p>
				@else
					@foreach ($posts as $item)
						<article>
							<header>
								<h2><a href="{{ route('/post/view/{id}', ['id' => $item->id]) }}">{{$item->title}}</a></h2>
								@auth
									@if ($user->id == $item->user_id)
										<a href="{{ route('/post/edit/{id}', ['id' => $item->id]) }}">Изменить</a>
									@endif
								@endauth
							</header>
							<p>{{$item->desc}}</p>
							<p>{!! $item->getHashtags() !!}</p>
						</article>
					@endforeach
				@endif
			</div>
		</div>
	</section>
	<section class="4u">
		<!-- Sidebar -->
		<section id="sidebar">
			<section>
				<h3>Поиск</h3>
				<form action="{{ route('/search') }}">
					<input name="keywords" placeholder="Текст" type="text" class="text" value="{{ @$_GET['keywords'] }}" />
				</form>
				<br>
				<h3>Поиск по хэштэгам</h3>
				<form action="{{ route('/search') }}">
					<input name="hashtags" placeholder="Хэштэги" type="text" class="text" value="{{ @$_GET['hashtags'] }}" />
				</form>
			</section>
		</section>
	</section>
</div>
@endsection
