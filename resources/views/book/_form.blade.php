<script src="//cdn.ckeditor.com/4.9.2/full/ckeditor.js"></script>
{!! Form::open(['url' => isset($model) ? '/book/update/'.$model->id : '/book/create', 'class' => 'form-horizontal']) !!}
	<div class="control-group">
		<label class="control-label">Название:</label>
		<div class="controls">
			<input type="text" class="span11" name="title" value="{{ isset($model) ? $model->title : ''}}" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Краткое описание:</label>
		<div class="controls">
			<textarea class="span11" name="desc">{{ isset($model) ? $model->desc : ''}}</textarea>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Книга:</label>
		<div class="controls">
			<textarea id="editor1" class="span11" name="data" rows="40">{{ isset($model) ? $model->data : ''}}</textarea>
		</div>
	</div>
	<div class="form-actions">
		<button type="submit" class="btn btn-success">Сохранить</button>
	</div>
{!! Form::close() !!}
<script>
	CKEDITOR.replace( 'editor1' );
</script>
