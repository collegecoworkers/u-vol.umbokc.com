{!! Form::open(['url' => isset($model) ? '/user/update/'.$model->id : '/user/create', 'class' => 'form-horizontal']) !!}

	<div class="control-group">
		<label class="control-label">Имя:</label>
		<div class="controls">
	    {!! Form::text('full_name', isset($model) ? $model->full_name : '', ['required' => '','class' => 'span11']) !!}
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">Логин:</label>
		<div class="controls">
	    {!! Form::text('name', isset($model) ? $model->name : '', ['required' => '','class' => 'span11']) !!}
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">Email:</label>
		<div class="controls">
	    {!! Form::text('email', isset($model) ? $model->email : '', ['required' => '','class' => 'span11']) !!}
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">Роль:</label>
		<div class="controls">
			{!! Form::select('role', $roles, isset($model) ? $model->role : '', ['required' => '','class' => 'span11']) !!}
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">Пароль:</label>
		<div class="controls">
	    {!! Form::password('password', ['class' => 'span11']) !!}
		</div>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Сохранить</button>
	</div>
{!! Form::close() !!}
