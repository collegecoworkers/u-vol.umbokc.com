<!DOCTYPE HTML>
<html ea>
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600" rel="stylesheet" type="text/css" />
	<link href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3" rel="stylesheet">
	<!--[if lte IE 8]><script src="{{ asset('assets/js/html5shiv.js') }}"><![endif]-->
	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/jquery.dropotron.js') }}"></script>
	<script src="{{ asset('assets/js/skel.min.js') }}"></script>
	<script src="{{ asset('assets/js/skel-panels.min.js') }}"></script>
	<script src="{{ asset('assets/js/init.js') }}"></script>
	<noscript>
		<link rel="stylesheet" href="{{ asset('assets/css/skel-noscript.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/style-n1.css') }}" />
	</noscript>
</head>
<body class="homepage" ov:h>
	<div id="header-wrapper" h:100vh ov:a>
		<div id="header" class="container">
			<h1 id="logo"><a href="/">{{ config('app.name', 'Laravel') }}</a></h1>
		</div>
		<section id="hero" class="container">
			@yield('content')
		</section>
	</div>
</body>
</html>
