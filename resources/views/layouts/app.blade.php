@php
	use App\User;
@endphp
<!DOCTYPE HTML>
<html ea>
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600" rel="stylesheet" type="text/css" />
	<link href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3" rel="stylesheet">
	<!--[if lte IE 8]><script src="{{ asset('assets/js/html5shiv.js') }}"><![endif]-->
	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/jquery.dropotron.js') }}"></script>
	<script src="{{ asset('assets/js/skel.min.js') }}"></script>
	<script src="{{ asset('assets/js/skel-panels.min.js') }}"></script>
	<script src="{{ asset('assets/js/init.js') }}"></script>
	<noscript>
		<link rel="stylesheet" href="{{ asset('assets/css/skel-noscript.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/style-n1.css') }}" />
	</noscript>
</head>
<body class="homepage">
	<div id="header-wrapper">

		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
		<div id="header" class="container">
			<h1 id="logo"><a href="/">{{ config('app.name', 'Laravel') }}</a></h1>
			<nav id="nav">
				<ul>
					<li><a href="{{ route('/post/add') }}">Добавить статью</a></li>
					@auth
						<li class="break" c#f>
							{{ User::curr()->name }}: <a style="padding: 0;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
						</li>
					@endauth
					@guest
						<li class="break"><a href="{{ route('login') }}" >Авторизация</a></li>
					@endguest
				</ul>
			</nav>
		</div>

		<section id="hero" class="container">
			<header>
				<h2>Модуль для автоматизации поиска по сайту</h2>
			</header>
		</section>
	</div>

	<div class="wrapper">
		<div class="container">
			@yield('content')
		</div>
	</div>

	<div id="footer-wrapper">
		<div id="copyright" class="container">
			<ul class="menu">
				<li>&copy; {{ config('app.name', 'Laravel') }}. All rights reserved.</li>
			</ul>
		</div>
	</div>
</body>
</html>
