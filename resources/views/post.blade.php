@extends('layouts.app')
@section('content')
<div class="row">
	<section class="8u ">
		<div class="row">
			<div class="u12">
				@if (!isset($post))
					<p>Посто нет</p>
				@else
					<article>
						<header>
							<h2>{{ $post->title }}</h2>
							@auth
								@if ($user->id == $post->user_id)
									<a href="{{ route('/post/edit/{id}', ['id' => $post->id]) }}">Изменить</a>
								@endif
							@endauth
						</header>
						<p>{!! $post->data !!}</p>
						<p>{!! $post->getHashtags() !!}</p>
					</article>
				@endif
			</div>
		</div>
	</section>
	<section class="4u">
		<!-- Sidebar -->
		<section id="sidebar">
			<section>
				<h3>Поиск</h3>
				<form>
					<input name="keywords" placeholder="Слова" type="text" class="text" />
				</form>
				<br>
				<h3>Поиск по хэштэгам</h3>
				<form>
					<input name="hashtag" placeholder="Хэштэг" type="text" class="text" />
				</form>
			</section>
		</section>
	</section>
</div>
@endsection
