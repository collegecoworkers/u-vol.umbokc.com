<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');
Route::get('/posts/{id}', 'SiteController@Posts')->name('/posts/{id}');
Route::get('/users', 'SiteController@Users')->name('/users');
Route::get('/search', 'SiteController@Search')->name('/search');

Route::get('/post/view/{id}', 'SiteController@Post')->name('/post/view/{id}');

Route::get('/post/add', 'PostController@Add')->name('/post/add');
Route::get('/post/edit/{id}', 'PostController@Edit')->name('/post/edit/{id}');
Route::get('/post/delete/{id}', 'PostController@Delete')->name('/post/delete/{id}');
Route::post('/post/create', 'PostController@Create')->name('/post/create');
Route::post('/post/update/{id}', 'PostController@Update')->name('/post/update/{id}');

Route::get('/user/add', 'UserController@Add')->name('/user/add');
Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
Route::post('/user/create', 'UserController@Create')->name('/user/create');
Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
